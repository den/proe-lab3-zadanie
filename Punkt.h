/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef PUNKT_H
#define PUNKT_H

#include <iostream>

class Punkt
{
	protected:
		double x;
		double y;
	public:
		void wypisz();

		Punkt();

		/**
		 * Konstruktor punktu
		 * @param {double} tmpx Wsp�rz�dna x
		 * @param {double} tmpy Wsp�rz�dna y
		 */
		Punkt(double tmpx, double tmpy);
		
		friend std::ostream & operator<< (std::ostream & os, const Punkt & p);
};

#endif
