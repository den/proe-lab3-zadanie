/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */
 
 #include <vector>
 #include "Lamana.h"
 #include "Punkt.h"
 
 Lamana::Lamana()
 {
	 
 }
 
 Lamana::Lamana(std::vector<Punkt> punkty)
 {
	 punkty_ = punkty;
 }
 
 void Lamana::wypisz()
 {
	std::cout << "Lamana o punktach zgiecia ";
	for(int i = 0; i < punkty_.size(); i++)
	{
		std::cout << punkty_[i];
		if(i == 1) std::cout << " i ";
		else std::cout << ", ";
	}
	std::cout << std::endl;
 }
 
 std::ostream & operator<< (std::ostream & os, const Lamana & l)
 {
	os << "Lamana o punktach zgiecia ";
	for(int i = 0; i < l.punkty_.size(); i++)
	{
		os << l.punkty_[i];
		if(i == 1) os << " i ";
		else os << ", ";
	}
	os << std::endl;
	
	return os;
 }
