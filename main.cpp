/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include <vector>
#include <memory>
#include "Fig.h"
#include "Trojkat.h"
#include "Lamana.h"
#include "Punkt.h"


const int NFIG=3;

int main()
{
	std::vector<Punkt> ptrojkata;
	ptrojkata.emplace_back(0, 0);
	ptrojkata.emplace_back(4, 0);
	ptrojkata.emplace_back(0, 5);
	
	std::vector<Punkt> plamana;
	plamana.emplace_back(0, 1);
	plamana.emplace_back(4, 2);
	plamana.emplace_back(3, 7);
	plamana.emplace_back(7, 1);
	
   std::vector<std::unique_ptr<Fig>> scena;
   //std::vector<std::unique_ptr<Fig>> scena;
   //std::unique_ptr<Fig> scena[NFIG];
   
   //scena[0] = new Trojkat(ptrojkata);
   //scena.push_back(new Trojkat(ptrojkata));
   scena.push_back(std::unique_ptr<Fig>(new Trojkat(ptrojkata)));
   scena.push_back(std::unique_ptr<Fig>(new Lamana(plamana)));
   //for(int i = 0; i < NFIG; i++ )
      //std::cout << *((Trojkat*)scena[0]); 
      //(scena[0]).wypisz(); 
   for(int i = 0; i < scena.size(); i++)
		scena[i]->wypisz();

   // Po mojemu to to jest ta magiczna linijka, bez kt�rej program zadzia�a, ale musi by�
   //for(int i = 0; i < NFIG; i++) delete scena[i];

   return 0;
}
