
/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */
 
 #ifndef LAMANA_H
 #define LAMANA_H
 
 #include <vector>
 #include "Punkt.h"
 #include "Fig.h"
 
 class Lamana: public Fig
 {
	 public:
		Lamana();
		Lamana(std::vector<Punkt> punkty);
		void wypisz();
		friend std::ostream & operator<< (std::ostream & os, const Lamana & l);
 };

#endif
