CC = g++
CCFLAGS = -std=c++11

lab3: main.o Fig.o Kolo.o Kwadrat.o Punkt.o Trojkat.o Lamana.o
	$(CC) main.o Fig.o Kolo.o Kwadrat.o Punkt.o Trojkat.o Lamana.o -o lab3 $(CCFLAGS)

main.o: main.cpp Fig.h Kolo.h Kwadrat.h Trojkat.h
	$(CC) -c main.cpp $(CCFLAGS)

Fig.o: Fig.h Fig.cpp Punkt.h
	$(CC) -c Fig.cpp $(CCFLAGS)

Kolo.o: Kolo.h Kolo.cpp Fig.h Punkt.h
	$(CC) -c Kolo.cpp $(CCFLAGS)

Kwadrat.o: Kwadrat.h Kwadrat.cpp Fig.h Punkt.h
	$(CC) -c Kwadrat.cpp $(CCFLAGS)

Punkt.o: Punkt.h Punkt.cpp
	$(CC) -c Punkt.cpp $(CCFLAGS)

Trojkat.o: Trojkat.h Trojkat.cpp Fig.h Punkt.h
	$(CC) -c Trojkat.cpp $(CCFLAGS)
	
Lamana.o: Lamana.h Lamana.cpp Fig.h Punkt.h
	$(CC) -c Lamana.cpp $(CCFLAGS)

clean:
	rm -f *.o
