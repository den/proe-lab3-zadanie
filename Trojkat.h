/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef TROJKAT_H
#define TROJKAT_H

#include <vector>
#include "Punkt.h"
#include "Lamana.h"

class Trojkat: public Lamana
{
	public:
		void wypisz();

		/**
		 * Konstruktor
		 * @param {Punkt} p1
		 * @param {Punkt} p2
		 * @param {Punkt} p3
		 */
		Trojkat(std::vector<Punkt> punkty);
};

#endif
