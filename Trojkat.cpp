/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include <vector>
#include <iostream>
#include "Trojkat.h"
#include "Punkt.h"

Trojkat::Trojkat(std::vector<Punkt> punkty)
{
	if(punkty.size() == 3)
	{
		punkty_ = punkty;
	}
	else
		std::cout << "ERROR: Nieprawidlowa ilosc punktow" << std::endl;
}

void Trojkat::wypisz()
{
	std::cout << "Trojkat o wierzcholkach ";
	for(int i = 0; i < punkty_.size(); i++)
	{
		punkty_[i].wypisz();
		if(i == 1) std::cout << " i ";
		else std::cout << ", ";
	}
	std::cout << std::endl;
}
