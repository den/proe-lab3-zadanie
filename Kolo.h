/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef KOLO_H
#define KOLO_H

#include "Punkt.h"
#include "Fig.h"

class Kolo: public Fig
{
	protected:
	   Punkt srodek;
	   double promien;
	public:
	   void wypisz();

	   /**
	    * Konstruktor ko�a
		* @param {Punkt} p obiekt punktu
		* @param {int} r
		*/
	   Kolo(Punkt p, int r);
};

#endif