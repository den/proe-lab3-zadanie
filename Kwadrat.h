/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef KWADRAT_H
#define KWADRAT_H

#include "Fig.h"
#include "Punkt.h"

class Kwadrat: public Fig
{
protected:
	Punkt wierzcholek;
	double bok;

public:
	void wypisz();

	/**
	 * Konstruktor kwadratu
	 * @param {Punkt}	p	Lewa g�rna kraw�d� kwadratu 
	 * @param {int}		bok D�ugo�� boku
	 */
	Kwadrat(Punkt p, double tmpbok);
};

#endif