/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include "Punkt.h"

Punkt::Punkt()
{

}

Punkt::Punkt(double tmpx, double tmpy)
{
	x = tmpx;
	y = tmpy;
}

void Punkt::wypisz()
{
	std::cout << "[" << x << ", " << y << "]";
}

std::ostream & operator<< (std::ostream & os, const Punkt & p)
{
	os << "[" << p.x << ", " << p.y << "]";
	
	return os;
}
