/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#ifndef FIG_H
#define FIG_H

#include <vector>
#include "Punkt.h"

class Fig
{
protected:
	std::vector<Punkt> punkty_;
public:
    virtual void wypisz() = 0;
    virtual ~Fig(){}
};

#endif
