/**
 * Lab 3
 * @author Marek Baranowski, 2T2
 */

#include <iostream>
#include "Kolo.h"
#include "Punkt.h"

Kolo::Kolo(Punkt p, int r)
{
	srodek = p;
	promien = r;
};

void Kolo::wypisz()
{
	std::cout << "Kolo o promieniu " << promien << " i wspolrzednych srodka ";
	srodek.wypisz();
	std::cout << std::endl;
}